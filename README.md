## Image Archiver

Creates a database dump of all images, including BLOB and EXIF data. Images can then be searched on using SQL queries.