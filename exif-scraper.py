import exifread, os, sys, re, sqlite3
# ToDo: Parse info from EXIF.


def getExif(filename, printOutput=False):
	image = open(filename, "rb")
	tags = exifread.process_file(image)
	try:
		tags.pop("JPEGThumbnail")
	except Exception:
		pass
	if printOutput:
		for tag in tags:
			print(f"{tag} : {tags[tag]}")
	image.close()
	return tags

def getAllImages(directory):
	images = []
	with os.scandir(directory) as files:
		for file in files:
			if file.is_file() and re.search("/.jpeg$|.JPEG$|.jpg$|.JPG$/gm", file.name):
				images.append(file.name)
	return images

def getBinaryFile(filename):
	file = open(filename, "rb")
	return file.read()

if __name__ == "__main__":
	verbose = False
	args = sys.argv

	if "-v" in args:
		verbose = True
	if "-h" in args:
		print("exif-scraper help:\n\t-v for verbose output.\n\t-h for help.")
		exit()
	if "-t" in args:
		timer = True

	db = sqlite3.connect("imageDump.db")
	db.execute("""
		CREATE TABLE IF NOT EXISTS images (
			file_name TEXT,
			EXIF TEXT,
			image_file BLOB,
			PRIMARY KEY (file_name)
		)
	
	""")

	cwd = os.getcwd()
	print(cwd)
	imageList = getAllImages(cwd)
	listLength = len(imageList)
	listCounter = 0
	print(f"Found {listLength} images to process...")



	for image in imageList:
		if verbose:
			print(f"Inserting Image: {image}")
		try:
			db.execute("INSERT INTO images VALUES (?,?,?)", (
				image, 
				str(getExif(image, printOutput=verbose)), 
				getBinaryFile(image)
				))
			db.commit()
		except Exception as e:
			print(f"Error inserting image {image}. Exception: {e}")
			listCounter += 1
		print(f"{int((listCounter / listLength)*1000)}% complete...")